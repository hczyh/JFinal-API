--#update(result)
    CREATE TABLE IF NOT EXISTS COMPANY (
       ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
       NAME           TEXT    NOT NULL,
       AGE            INT     NOT NULL,
       ADDRESS        CHAR(50),
       SALARY         REAL
    )
--#end
--SQLite 创建表：受影响行数#(result)


--#for(i = 0; i < 10; i++)
    --#update(resultFor)
        INSERT INTO COMPANY (NAME,AGE,ADDRESS,SALARY)
        VALUES ('Paul', 32, #para('I=' + i), 20000.00 )
    --#end
    --SQLite Insert 语句：(#(i))受影响行数#(resultFor)
--#end


--### list是定义集合的变量名，可以不写，默认为list
--#find(list)
    SELECT * FROM COMPANY
--#end
--SQLite Select 语句: list=#(list)


--### --返回结果
--#(this.jsonData(list))