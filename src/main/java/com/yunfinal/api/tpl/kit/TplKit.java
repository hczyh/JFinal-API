package com.yunfinal.api.tpl.kit;

import com.jfinal.ext.kit.DateKit;
import com.jfinal.kit.HashKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.DbPro;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;
import com.yunfinal.api.tpl.template.directive.db.DbConfigName;

import java.util.Date;
import java.util.Map;
import java.util.Objects;

/**
 * 配合UI 操作 的常用 公共函数
 */
public class TplKit {

    /**
     * 把字符串切割为TplDateSlot，start 和 end
     * @param str " - " 分隔的字符串
     * @return TplDateSlot
     */
    public TplDateSlot splitToDate(String str) {
        String[] splitStr = str.split(" \\- ");
        return new TplDateSlot(splitStr);
    }

    /**
     * 转换为Record对象
     */
    public Record toRecord(Object obj){
        if (Objects.isNull(obj)){
            return new Record();
        }
        if(obj instanceof Record){
            return (Record) obj;
        }
        if (obj instanceof Map){
            return new Record().setColumns((Map)obj);
        }
        if (obj instanceof Model){
            return new Record().setColumns((Model)obj);
        }
        throw new RuntimeException("参数obj非法类型，需Record或者Map或者Model对象");
    }
    public Record toRecord(){
        return new Record();
    }

    /**
     * 根据ID识别 是否是 dbSave 还是 dbUpdate
     */
    public boolean dbSaveOrUpdateById(String tableName, Object obj){
        Record r = toRecord(obj);
        if(StrKit.isBlank(r.getStr("id"))) {
            return dbSave(tableName, r);
        }
        return dbUpdate(tableName, obj);
    }
    public boolean dbSave(String tableName, Object obj){
        String id = "id";
        Record r = toRecord(obj);
        r.remove(id);
        return dbSaves(tableName, id, r);
    }
    private DbPro db(){
       return DbConfigName.db();
    }

    public boolean dbSaves(String tableName, String key, Object obj){
        return db().save(tableName, key, toRecord(obj));
    }

    public boolean dbUpdate(String tableName, Object obj){
        return dbUpdates(tableName, "id", obj);
    }

    public boolean dbUpdates(String tableName, String key, Object obj){
        return db().update(tableName, key, toRecord(obj));
    }

    /**
     * UI经常会使用 一个值包含开始和结束日期，所以做一个区间日期 包装类，方便使用
     */
    public class TplDateSlot {
        private String start;
        private String end;

        public TplDateSlot() {
        }

        public TplDateSlot(String[] arr) {
            if (arr != null && arr.length > 1){
                this.start = arr[0];
                this.end = arr[1];
            }
        }

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }

        public Date getStartDate() {
            return DateKit.toDate(start);
        }

        public Date getEndDate() {
            return DateKit.toDate(end);
        }
    }
}
