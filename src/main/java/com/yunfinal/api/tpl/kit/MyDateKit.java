package com.yunfinal.api.tpl.kit;

import com.jfinal.ext.kit.DateKit;
import com.jfinal.kit.TimeKit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * 自动识别日期对象，字符串自动转Date，Date自动转字符串
 * （Date 转字符串 时 不用做判断，可直接传入，会自动返回null，节省编写代码）
 * @author  杜福忠
 */
public class MyDateKit extends DateKit {

    public static Object to(Object d){
        if (Objects.isNull(d)){
            return null;
        }
        if (d instanceof Date){
            return toStr((Date)d);
        }
        return toDate(d.toString());
    }

    public static Object to(Object d, String pattern) {
        if (Objects.isNull(d) || Objects.isNull(pattern) ){
            return null;
        }
        if (d instanceof Date){
            return toStr((Date)d, pattern);
        }
        SimpleDateFormat sdf = TimeKit.getSimpleDateFormat(pattern);
        try {
            return sdf.parse(d.toString().trim());
        } catch (ParseException e) {
            return toDate(d.toString());
        }
    }
}
