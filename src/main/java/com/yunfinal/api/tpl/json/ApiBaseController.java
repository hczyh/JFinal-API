package com.yunfinal.api.tpl.json;

import com.jfinal.core.Controller;
import com.jfinal.core.NotAction;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.HashMap;
import java.util.Map;

public class ApiBaseController extends Controller {

    @NotAction
    public boolean isContentTypeJson() {
        String contentType = getRequest().getContentType();
        return "application/json".equalsIgnoreCase(contentType) ? true : "text/json".equalsIgnoreCase(contentType);
    }

    /**
     * 注意本方法调用，并不会直接结束你的函数，需要使用 return
     */
    @NotAction
    public Object jsonPage(Page<Record> page) {
        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("msg", "OK");
        map.put("count", page.getTotalRow());
        map.put("pageNumber", page.getPageNumber());
        map.put("pageSize", page.getPageSize());
        map.put("data", page.getList());
        renderJson(map);
        return null;
    }

    /**
     * 注意本方法调用，并不会直接结束你的函数，需要使用 return
     */
    @NotAction
    public Object jsonData() {
        return jsonData(null);
    }

    /**
     * 注意本方法调用，并不会直接结束你的函数，需要使用 return
     * 返回JSON参数
     */
    @NotAction
    public Object jsonData(Object data) {
        return json(0, "OK", data);
    }

    @NotAction
    public Object jsonData(String msg, Object data) {
        return json(0, msg, data);
    }

    /**
     * 注意本方法调用，并不会直接结束你的函数，如果需要不执行你后续的代码，应该配合使用 return 结束方法jsonError();这样子
     */
    @NotAction
    public Object jsonError() {
        return jsonError(-1, "fail");
    }

    /**
     * 注意本方法调用，并不会直接结束你的函数，如果需要不执行你后续的代码，应该配合使用 return 结束方法jsonError(500, "XX");这样子
     */
    @NotAction
    public Object jsonError(int code, String msg) {
        return jsonError(code, msg, null);
    }

    @NotAction
    public Object jsonError(int code, String msg, Object data) {
        return json(code, msg, data);
    }

    @NotAction
    public Object json(int code, String msg, Object data) {
        Map<String, Object> map = new HashMap<>();
        map.put("code", code);
        map.put("msg", msg);
        if (data != null) {
            map.put("data", data);
        }
        renderJson(map);
        return null;
    }

    //可以扩展 获取登录用户对象 等功能
}
